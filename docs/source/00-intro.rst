===============================
 Basic (non-recursive) schemata
===============================

Overview
========

This package allows to serialize/deserialize data in a JSON friendly manner by
using and manipulating types.

It features a `type system <xotl.plato.types>`:any: that is extensible and it
has been specifically designed to avoid non-termination issues with recursive
data.  In a word, we don't allow to create recursive types; any notion of
recursion is not part of the type system itself.

Having these types, we can now cast `dataclasses`:mod: into `schemata
<xotl.plato.schema>`:any: by attaching a `type <xotl.plato.types>`:any: to
them.  We can automatically build the type of most basic Python types,
enumerations and other.


The way to Pydantic_
====================

As of April, 2024 we have decided to deprecate ``xotl.plato`` and proceed to
use Pydantic_ version 2.  The specific plan how to perform this is not
completely clear.  We're considering whether or not to implement
``xotl.plato`` using pydantic internally.

Most of the code in this package comes from two projects we have worked
before.  It might be easier to switch to Pydantic_ directly without any direct
support from ``xotl.plato``.

.. _Pydantic: https://pypi.org/project/pydantic/
