.. xotl.plato documentation master file, created by
   sphinx-quickstart on Sun Mar 20 16:36:33 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xotl.plato's documentation!
======================================

.. warning::

   As of April, 2024 we have decided to deprecate ``xotl.plato`` and proceed
   to use Pydantic_ version 2.  The specific plan how to perform this is not
   completely clear.  We're considering whether or not to implement
   ``xotl.plato`` using pydantic internally, to get the benefits of pydantic
   while having time to update our projects that use ``xotl.plato``.

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Contents:

   00-intro
   api/*
   history


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

 .. _Pydantic: https://pypi.org/project/pydantic/
